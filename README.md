`curl <image_link> --output <dest_file_name>`

`curl https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Oxygen480-apps-kpresenter.svg/1200px-Oxygen480-apps-kpresenter.svg.png --output sample.png`

`convert sample.png -define icon:auto-resize=256,64,48,32,16 favicon.ico`

`convert public/sample.png -resize 192x192 icon192.png`

`convert public/sample.png -resize 512x512 icon512.png`

need to understand what is favicon, what is maskable...only one maskable item is required in icons array. maskable image needs to be generated
