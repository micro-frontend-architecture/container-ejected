import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import {Nav} from './components/Nav';
import {Demopage} from './components/Demopage';
import {Home} from './components/Home';


export const App = () => {
    return (
      <Router>
      <Nav />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/demopage' element={<Demopage />} />          
        </Routes>
      </Router>
    );
}

