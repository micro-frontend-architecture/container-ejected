import React,  { Suspense }  from 'react';
import ErrorBoundary from "./ErrorBoundary";
// import {lazyWithRetry} from "./helper";

// const Mfe1App = lazyWithRetry(() => import("mfe1/App"));
const Mfe1App = React.lazy(() => import("mfe1/App"));
const Mfe2App = React.lazy(() => import("mfe2/App"));

const RemoteWrapper = ({ children }) => (
    <div
      style={{
        border: "1px solid red",
        background: "white",
      }}
    >
      <ErrorBoundary>{children}</ErrorBoundary>
    </div>
);

export const  Home = () => {
  return (
    <div>
        <h1 className="container">Hi, I am from main app.</h1>
        <Suspense fallback={<div>Loading... </div>}>
            <RemoteWrapper>
                <Mfe1App />
            </RemoteWrapper>
            <RemoteWrapper>
                <Mfe2App />
            </RemoteWrapper>
        </Suspense>    
    </div>
  );
}

